﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu1 : MonoBehaviour
{

    public static bool GameIsPause = false;
    // public GameObject HideHPBar;
    //public GameObject HideCursor;

    public GameObject PauseMenuUI;

    // Update is called once per frame

    void Start()
    {
        Resume();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //Cursor.lockState = CursorLockMode.None;
            if (GameIsPause)
            {
                Resume();
            }
            else
            {

                Pause();
            }
        }

    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPause = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        // HideHPBar.GetComponent<Payer_HP>().enabled = true;
    }

    void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPause = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        // HideHPBar.GetComponent<Payer_HP>().enabled = false;
        ////HideCursor.GetComponent<FreeLookCam>().enabled = false;


    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit");
    }

}
