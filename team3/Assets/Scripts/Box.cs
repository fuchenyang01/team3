﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour {

    public GameObject item;
    private GameObject playerUnit;
    private float diatanceToPlayer;
    // Use this for initialization
    void Start () {
        playerUnit = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
        diatanceToPlayer = Vector3.Distance(playerUnit.transform.position, transform.position);
        if (diatanceToPlayer < 2)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Instantiate(item, new Vector3(this.transform.position.x, 1, this.transform.position.z), Quaternion.identity);
                Destroy(this.gameObject);
            }
        }
    }
     void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            
        }
    }
}
