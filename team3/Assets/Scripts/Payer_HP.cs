﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Payer_HP : MonoBehaviour {
    private AudioSource AudioSource;
    //玩家最大血量
    public int maxHealth = 100;
    //玩家当前血量
    public int curHealth { get; set; }
    
    //血条的长度
    public float healthBarLength;

    public AudioClip hitSound;

    void Start()
    {
        curHealth = maxHealth;
        //设置血条的长度等于屏幕宽度的一半
        healthBarLength = Screen.width / 2;
        AudioSource = GetComponent<AudioSource>();
    }


    void Update()
    {
        //使用自定义调节当前血量的方法
        AdjustCurHealth(0);
    }
    void OnGUI()
    {
        //使用GUI.Box绘制血条
        GUI.Box(new Rect(10, 10, healthBarLength, 20), curHealth + "/" + maxHealth);
        
    }
    //自定义调节当前血量的方法
    public void AdjustCurHealth(int adj)
    {
        curHealth += adj;
        //判断当前血量是否小于零，如果小于零，则设置当前血量等于零
        if (curHealth <= 0)
        {
            Application.LoadLevel(0);
        }
       
        //判断当前血量是否大于最大血量，如果大于最大血量，则设置当前血量等于最大血量
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }
        //判断最大血量是否小于1，如果小于1，则设置最大血量等于1
        if (maxHealth < 1)
        {
            maxHealth = 1;
        }
        //血条值的变化
        healthBarLength = (Screen.width / 2) * (curHealth / (float)maxHealth);
    }

    private void PlayhitSound()
    {
        AudioSource.clip = hitSound;
        AudioSource.Play();
    }

    // 碰撞开始
    void OnCollisionEnter(Collision collision)
    {
        if( collision.gameObject.tag == "Enemy")
         {
             Debug.Log("-10");
             AdjustCurHealth(-10);
            PlayhitSound();
         }
       
    }

     // 碰撞结束
     void OnCollisionExit(Collision collision)
    {

     }
    // 碰撞持续中
    void OnCollisionStay(Collision collision)
    {
       
    }
    private void OnTriggerEnter(Collider collision)
    {
        
    }

}
