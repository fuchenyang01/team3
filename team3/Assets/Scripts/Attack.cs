﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {
    public Camera _camera;
    public AudioClip shootSound;
    public AudioClip punchSound;
    private AudioSource AudioSource;
    private string healthString;
    private int bulletcount;
    // Use this for initialization
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }
    void OnGUI()
    {
        int size = 12;
        float posX = _camera.pixelWidth / 2 - size / 4;
        float posY = _camera.pixelHeight / 2 - size / 2;
        GUI.Label(new Rect(posX, posY, size, size), "*");

        GUI.Label(new Rect(600, 200, 100, 20), healthString);

        GUI.skin.label.normal.textColor = new Vector4(255, 0, 0, 1);
    }
    // Update is called once per frame
    void Update()
    {

    }
    private IEnumerator F(Vector3 pos)//协程
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.AddComponent<Rigidbody>();
        Vector3 force = (pos - _camera.transform.position) * 200;
        sphere.GetComponent<Rigidbody>().AddForce(force);
        sphere.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        sphere.transform.position = pos;
        
        yield return new WaitForSeconds(1);
        Destroy(sphere);//1s后消失
        healthString = "";
    }

    private void PlayShootSound()
    {
        AudioSource.clip = shootSound;
        AudioSource.Play();
    }

    private void PlayPunchSound()
    {
        AudioSource.clip = punchSound;
        AudioSource.Play();
    }

    private void Punching(Collider collision)
    {
       if (Input.GetMouseButtonDown(0))
        {
            ReactiveTarget RT = collision.GetComponent<ReactiveTarget>();
            Vector3 force = (collision.GetComponent<Transform>().position - _camera.transform.position) * 200;
            collision.GetComponent<Rigidbody>().AddForce(force);
            PlayPunchSound();
            RT.Reacttohit();//调用敌人的被击反馈
            healthString = RT.gethealthString();

        }
    }

    public void shooting()
    {
       
            PlayShootSound();
            Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);
            Ray ray = _camera.ScreenPointToRay(point);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))//hit就是被击中的目标
            {
                // Debug.Log(point);
                //获取打到的物体
                GameObject hitobj = hit.transform.gameObject;
                ReactiveTarget RT = hitobj.GetComponent<ReactiveTarget>();
                if (RT != null)
                {
                    RT.Reacttohit();//调用敌人的被击反馈
                    healthString = RT.gethealthString();
                    StartCoroutine(F(hit.point));
                }
                else
                {
                    StartCoroutine(F(hit.point));//用协程造子弹，因为要编写子弹要1s后消失的效果
                }
            }
        
    }

    private void OnTriggerStay(Collider collision)
    {
        
        if (collision.gameObject.tag == "Enemy")
        {
           
            Punching(collision);

        }
    }
    private void OnTriggerEnter(Collider collision)
    {

        
    }
    private void OnTriggerExit(Collider collision)
    {

       
    }
}
