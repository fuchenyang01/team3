﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour {

    public GameObject bullet;
    public GameObject medikit;
    public GameObject enemy;
    public GameObject boss;
    private GameObject spotlightObj;
    private Light spotlight;
    private bool lightswith;
    // Use this for initialization
    void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        lightswith = false;
        spotlightObj = GameObject.FindGameObjectWithTag("Spotlight");
        spotlight = spotlightObj.GetComponent<Light>();
        for (int i = 0; i < 10; i++)

        {
            Instantiate(bullet, new Vector3(Random.Range(-47f,47f), 1, Random.Range(-18f, 23f)), Quaternion.identity);
            Instantiate(medikit, new Vector3(Random.Range(-47f, 47f), 1, Random.Range(-18f, 23f)), Quaternion.identity);
           // Instantiate(enemy, new Vector3(Random.Range(-47f, 47f), 1, Random.Range(-18f, 23f)), Quaternion.identity);
           // Instantiate(boss, new Vector3(Random.Range(-47f, 47f), 1, Random.Range(-18f, 23f)), Quaternion.identity);
        }

    }
	
	// Update is called once per frame
	void Update () {
        
       // Cursor.lockState = CursorLockMode.Locked;
        Object body = GameObject.FindWithTag("Finish");
        Destroy(body, 5);
        if (Input.GetKeyDown(KeyCode.E))
        {
 
            if (lightswith == false)
            {
                lightswith = true;
                spotlight.intensity = 1;
            }
            else 
            {
                lightswith = false;
                spotlight.intensity = 0;
            }
        }
      
    }

    void OnGUI()
    {


    }
}
