﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {
   
    GameObject[] door;
    GameObject thisdoor;
    public AudioClip doorSound;
    private AudioSource AudioSource;
    // Use this for initialization
    void Start () {
        door = GameObject.FindGameObjectsWithTag("Door");
        thisdoor = null;
        AudioSource = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
       

    }
    public void opendoor()
    {
      
         thisdoor.GetComponent<Animator>().SetBool("open", true);
        PlaydoorSound();

    }
    void closedoor()
    {
        thisdoor.GetComponent<Animator>().SetBool("open", false);
    }

    private void PlaydoorSound()
    {
        AudioSource.clip = doorSound;
        AudioSource.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i < door.Length; i++)
        {
            if (door[i].GetComponent<Collider>() == other)
            {
                thisdoor = door[i];
                thisdoor.tag = "thisDoor";
            }
        }
       
        
    }
    private void OnTriggerStay(Collider other)
    {
        
    }
    private void OnTriggerExit(Collider other)
    {       
            if (other.gameObject.tag == "thisDoor")
            {
                GameObject.FindGameObjectWithTag("thisDoor").tag = "Door";

                //closedoor();
            }        
    }
}
