﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOffScript : MonoBehaviour
{
    public Light myLight;

    public bool LightflahingRunning = true;
    public bool LightTurnOffRunning = true;

    public float FlashingLightTimer = 0.1f; // 0.1f for how fast light will be flashing
    public float FlashingLightCoutdown = 120f; // 120f is 2min countdown when game started. and when it reach to 0 then flashing light will start.


    public float TurnOffLightTimer = 1f;      // 1f for how long the light will turn off after flashing light
    public float TurnOffLightCountdown = 360f;  // 360 is 6 min but flashing light will take 4min and then slowly light off

    void Update()
    {
 
        // Timer for light flashing will Running.
        if (LightflahingRunning == true)
        {
            FlashingLightCoutdown -= Time.deltaTime;
        }

        // Timer for light flashing will Running.
        if (LightTurnOffRunning == true)
        {
            TurnOffLightCountdown -= Time.deltaTime;
        }

        // Generate light intensity light if the timer keep reach 0
        if (FlashingLightCoutdown <= 0f)
        {
            myLight.intensity = Random.Range(0.1f, 1f);
            FlashingLightCoutdown = FlashingLightTimer;
            //Debug.Log("Time reset");
        }

        // Take away 0.06 from current light intensity if timer keep reach 0.
        else if (TurnOffLightCountdown <= 0f)
        {
            myLight.intensity -= 0.06f; // Take away light intensity
            LightflahingRunning = false; // Turn off the flahing light countdown
            TurnOffLightCountdown = TurnOffLightTimer; // reset timer for light turn off. 
        }

        if (myLight.intensity <= 0f)
        {
            myLight.intensity = 0f;
            LightTurnOffRunning = false;
        }

    }
}