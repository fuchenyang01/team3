using UnityEngine;
using System.Collections;

public class RandomFloorBotMovement : MonoBehaviour
{
	//public Vector2 minimumCoordinates;
	//public Vector2 maximumCoordinates;
	public Vector2 speedVariance;
	public Vector2 newDestinationTimeVariance;
	public Vector2 newSpeedTimeVariance;

    private GameObject playerUnit;
    private UnityEngine.AI.NavMeshAgent nav;
	private float newDestinationTime;
	private float destinationTimer;
	private float newSpeedTime;
	private float speedTimer;

    private void Start()
    {
        playerUnit = GameObject.FindGameObjectWithTag("Player");
    }
    void Awake ()
	{
		nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
		SetDestination();
		SetRandomSpeed();
	}
	

	void FixedUpdate ()
	{
		destinationTimer += Time.deltaTime;
		speedTimer += Time.deltaTime;
		
		if(destinationTimer >= newDestinationTime)
			SetDestination();
		
		if(speedTimer >= newSpeedTime)
			SetRandomSpeed();
	}
	
	
	void SetDestination ()
	{
        //float newX = Random.Range(minimumCoordinates.x, maximumCoordinates.x);
        //float newZ = Random.Range(minimumCoordinates.y, maximumCoordinates.y);
        float playerX = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position.x;
        float playerY = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position.y;
        if((Mathf.Pow((playerX - this.transform.position.x),2)+ Mathf.Pow((playerY - this.transform.position.y), 2)) < 100)
        {
            nav.destination = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
        }
       
		SetNewDestinationTime();
	}
	
	void SetRandomSpeed ()
	{
		nav.speed = Random.Range(speedVariance.x, speedVariance.y);
		SetNewSpeedTime();
	}
	
	void SetNewDestinationTime ()
	{
		newDestinationTime = Random.Range(newDestinationTimeVariance.x, newDestinationTimeVariance.y);
		destinationTimer = 0f;
	}
	
	
	void SetNewSpeedTime ()
	{
		newSpeedTime = Random.Range(newSpeedTimeVariance.x, newSpeedTimeVariance.y);
		speedTimer = 0f;
	}
}
